## Build status 🏗
![Build Status](https://gitlab.com/tonecoach/tonecoach-website/badges/master/build.svg)

---

## Build setup

### Clone and install dependencies
```
git@gitlab.com:tonecoach/tonecoach-website.git

npm i
```

### Serve with hot reload at localhost:3000
```
npm run dev
```
### Generate static project
```
npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

<small>* This project does not currently use the `build` or `start` scripts that come with nuxt.js - this website is statically rendered with the `generate` script.</small>

## ToneCoach public website 🌐

> Fun and simple ear training for musicians and audio engineers 👂 https://tone.coach

This repo contains the code powering the [ToneCoach website](https://tone.coach). It uses pugjs as a templating engine and is served via GitLab pages. Any code and assets in the public folder are available to the internet!

## GitLab pages CD setup 🚢

Below is the GitLab pipeline configuration for this code. It uses `pug-cli` to render the pages in the /public folder to HTML files for serving. After the .pug has been rendered, it removes the raw .pug files.

```
image: node:latest

cache:
  paths:
    - node_modules/

pages:
  stage: deploy
  script:
    - npm i
    - npm run generate
    - mv dist public
  artifacts:
    paths:
    - public
  only:
  - master
```

The above configuration is saved to the `.gitlab-ci.yml` file at the root of this repo.

## Issues ✨

Any future plans, improvements, and bugs should be submitted to the [repository's issue list](https://gitlab.com/tonecoach/tonecoach-website/issues). We also take suggestions for improving the site! Just use the suggestion tag as well as any other appropriate tag(s) for the issue you're submitting.

## Contributing 🖐

Thanks for considering helping ToneCoach! Although we appreciate the thought, no coding help is needed at this time with this particular project. If you have a suggestion, though, see the issues section above to see how you can submit it.

## Looking for the ToneCoach app? 📱

The ToneCoach client application is currently a private repository on GitLab, however you can check out the live app and start ear training by following [this link](https://app.tone.coach). You can also visit the [ToneCoach site](https://tone.coach) for more links to download the app on your mobile device!

## Check out ToneCoach Lite! 🎈

...coming soon 😉
