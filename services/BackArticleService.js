export default {
  // looks in the /articles folder for all .md files
  async getArticleFilepaths() {
    const glob = require('glob')
    let filepaths = glob.sync( '**/*.md' , { cwd: 'articles' })

    if (Array.isArray(filepaths) && filepaths.length) return filepaths
    else return [] //'No article files!'
  },

  // prepends /courtside/ to all available filepaths to build routes
  async getArticleEndpoints() {
    let filepaths = await this.getArticleFilepaths(),
        article_endpoints = filepaths.map((filepath, _) => {
          let endpoint = filepath.substr(0, filepath.lastIndexOf('.'))

          return `/courtside/${endpoint}`
        })

    return article_endpoints
  },

  // uses filepaths to build a unique array of article categories
  async getArticleCategories() {
    let filepaths = await this.getArticleFilepaths(),
        categories = filepaths.map(filepath => {
          return filepath.split('/')[0]
        })

    return categories.filter(
      (value, index, self) => {
        return self.indexOf(value) === index;
      }
    )
  },

  // uses filepaths to build an array of article slugs
  async getArticleSlugs() {
    let filepaths = await this.getArticleFilepaths(),
        slugs = filepaths.map(filepath => {
          return filepath.split('/')[1]
        })

    return slugs
  }
}
