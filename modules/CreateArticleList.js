const fs = require('fs')
const path = require('path')
let current_directory = __dirname

import BackArticleService from "../services/BackArticleService.js"

export default async function() {
  let article_filepaths = await BackArticleService.getArticleFilepaths(),
      article_categories = await BackArticleService.getArticleCategories()

  let filepath_splits = article_filepaths.map(article_filepath => {
    return article_filepath.split("/")
  })

  let category_slugs = article_categories.map(category => {
    let category_slugs = filepath_splits.filter(split => {
      return category === split[0]
    }).map(split => {
      return split[1].replace(/\.md/, "") // get the article slug
    })

    return category_slugs
  })

  let article_list = {}

  for (let index in article_categories) {
    let category = article_categories[index]
    article_list[category] = category_slugs[index]
  }

  let filepath = path.join(current_directory, "..", "articles/article_list.json")
  fs.writeFile(filepath, JSON.stringify(article_list, null, 2), function(err) {
    if (err) throw err
    console.log('The article list file was created successfully.')
  })
}
