const path = require('path')
import ArticleService from './services/BackArticleService.js'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'ToneCoach | Fun and simple ear training',
    meta: [
      // Essential SEO metadata
      {
        hid: "Content-Type",
        "http-equiv": "Content-Type",
        content: "text/html; charset=utf-8"
      },
      {
        hid: "X-UA-Compatible",
        "http-equiv": "X-UA-Compatible",
        content: "IE=edge,chrome=1"
      },
      {
        hid: "image",
        itemprop: "image",
        content: "https://tone.coach/img/banner.png"
      },
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      // Mobile colors
      {
        hid: "msapplication-TileColor",
        name: "msapplication-TileColor",
        content: "#00BCD4"
      },
      {
        hid: "msapplication-TileImage",
        name: "msapplication-TileImage",
        content: "/favicon/ms-icon-144x144.png"
      },
      {
        hid: "theme-color",
        name: "theme-color",
        content: "#00BCD4"
      },
      // Twitter metadata
      {
        hid: "twitter:site",
        property: "twitter:site",
        content: "@TeamToneCoach"
      },
      {
        hid: "twitter:creator",
        property: "twitter:creator",
        content: "@TeamToneCoach"
      },
      // Twitter app data
      // {
      //   hid: "apple-itunes-app",
      //   property: "apple-itunes-app",
      //   content: "app-id=xxxxxxxxx"
      // },
      {
        hid: "twitter:app:name:iphone",
        property: "twitter:app:name:iphone",
        content: "ToneCoach | Fun and simple ear training"
      },
      // {
      //   hid: "twitter:app:id:iphone",
      //   property: "twitter:app:id:iphone",
      //   content: "xxxxxxxxx"
      // },
      {
        hid: "twitter:app:name:ipad",
        property: "twitter:app:name:ipad",
        content: "ToneCoach | Fun and simple ear training"
      },
      // {
      //   hid: "twitter:app:id:ipad",
      //   property: "twitter:app:id:ipad",
      //   content: "xxxxxxxxx"
      // },
      {
        hid: "twitter:app:name:googleplay",
        property: "twitter:app:name:googleplay",
        content: "ToneCoach Mobile"
      },
      {
        hid: "twitter:app:id:googleplay",
        property: "twitter:app:id:googleplay",
        content: "coach.tone.train"
      }
    ],
    link: [
      // Favicons
      {rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico'},
      {rel: "manifest", href: "/favicon/manifest.json"},
      {rel: "apple-touch-icon", sizes: "57x57", href: "/favicon/apple-icon-57x57.png"},
      {rel: "apple-touch-icon", sizes: "60x60", href: "/favicon/apple-icon-60x60.png"},
      {rel: "apple-touch-icon", sizes: "72x72", href: "/favicon/apple-icon-72x72.png"},
      {rel: "apple-touch-icon", sizes: "76x76", href: "/favicon/apple-icon-76x76.png"},
      {rel: "apple-touch-icon", sizes: "114x114", href: "/favicon/apple-icon-114x114.png"},
      {rel: "apple-touch-icon", sizes: "120x120", href: "/favicon/apple-icon-120x120.png"},
      {rel: "apple-touch-icon", sizes: "144x144", href: "/favicon/apple-icon-144x144.png"},
      {rel: "apple-touch-icon", sizes: "152x152", href: "/favicon/apple-icon-152x152.png"},
      {rel: "apple-touch-icon", sizes: "180x180", href: "/favicon/apple-icon-180x180.png"},
      {rel: "icon", type: "image/png", sizes: "192x192", href: "/favicon/android-icon-192x192.png"},
      {rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon/favicon-32x32.png"},
      {rel: "icon", type: "image/png", sizes: "96x96", href: "/favicon/favicon-96x96.png"},
      {rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon/favicon-16x16.png"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/fonts.css',
    '~/assets/css/global.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/google-analytics.js', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/sitemap',
    '~/modules/CreateArticleList.js'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    splitChunks: {
     layouts: true
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.md$/,
        loader: 'raw-loader',
        include: path.resolve('articles')
      })

      config.module.rules.push({
        test: /\.pug$/,
        loader: 'pug-plain-loader'
      })
    }
  },
  /*
  ** Sitemap generation
  */
  sitemap: {
    hostname: 'http://tone.coach',
    gzip: false,
    lastmod: `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDay() + 1}`,
    routes: async function() {
      let article_endpoints = await ArticleService.getArticleEndpoints(),
          category_endpoints = await ArticleService.getArticleCategories()

      return []
      .concat(article_endpoints)
      .concat(category_endpoints.map(category => { return `/courtside/${category}`}))
      .concat(['404'])
    }
  },
  /*
  ** Static page generation
  */
  generate: {
    routes: async function() {
      let article_endpoints = await ArticleService.getArticleEndpoints(),
          category_endpoints = await ArticleService.getArticleCategories()

      return []
      .concat(article_endpoints)
      .concat(category_endpoints.map(category => { return `/courtside/${category}`}))
      .concat(['404'])
    }
  }
}
